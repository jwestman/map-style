<!-- Please fill out the form as completely as you can, but it's okay if not all the sections apply to your request, or if you're not sure about some of them. Also, please leave all headers and italicized directions intact (even if you don't fill them out). -->

# Change Request

*Describe the feature(s) you'd like to change and how you'd like to change them.*

## Motivation
*Describe the problem you're trying to solve with this change. Who is
experiencing the problem? Are there other ways of solving it?*

## Prior Art
*Upload screenshots of how this feature looks in other maps. Describe the
notable similarities and differences, and anything else that stands out. If
the change is a departure from existing maps, explain why you think it is
justified.*

*Some maps you can check (you don't have to include all of these):*
- *[Google Maps](https://www.google.com/maps)*
- *[Bing Maps](https://www.bing.com/maps/)*
- *Apple Maps, if available*
- *[OpenStreetMap Carto](https://www.openstreetmap.org)*
- *[Qwant Maps](https://www.qwant.com/maps)*
- *Maps specialized for the feature you're requesting (e.g. topographic maps,
official government or park maps, etc.)*

## Internationalization
*Describe how this change applies internationally, across languages, countries, and cultures. Describe what efforts (if any) will be required to make this change applicable worldwide.*

## Data Source
*If the change requires additional data, link the relevant section of [the documentation](https://openmaptiles.org/schema/) here. If changes are needed to OpenMapTiles, describe those changes.*
