Please choose an issue template from the dropdown above.

- **Feature Request**: To request a new feature be displayed on the map.
- **Change Request**: To request a change to a feature or set of features that
is already displayed on the map.
- **Discussion**: To bring up a broad idea or discussion without a specific
change in mind.
- **Technical Issue**: To report a technical (rather than design-related) issue.
