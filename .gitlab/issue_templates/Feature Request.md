<!-- Please fill out the form as completely as you can, but it's okay if not all the sections apply to your request, or if you're not sure about some of them. Also, please leave all headers and italicized directions intact (even if you don't fill them out). -->

# Feature Request

*Describe the feature(s) you'd like to see displayed on the map.*

## Prior Art
*Upload screenshots of how this feature looks in other maps. Describe the
notable similarities and differences, and anything else that stands out.*

*Some maps you can check (you don't have to include all of these):*
- *[Google Maps](https://www.google.com/maps)*
- *[Bing Maps](https://www.bing.com/maps/)*
- *Apple Maps, if available*
- *[OpenStreetMap Carto](https://www.openstreetmap.org)*
- *[Qwant Maps](https://www.qwant.com/maps)*
- *Maps specialized for the feature you're requesting (e.g. topographic maps,
official government or park maps, etc.)*

## Suggested Rendering
*If you have ideas about how to render this feature, describe them here. Remember that design is an iterative process and not all suggestions may end up being used.*

## Internationalization
*Describe how this feature applies internationally, across languages, countries, and cultures. Describe what efforts (if any) will be required to make this feature usable worldwide.*

## Data Source
*If the feature is already part of the OpenMapTiles schema, link the relevant section of [the documentation](https://openmaptiles.org/schema/) here. If changes are needed to OpenMapTiles, describe those changes.*
