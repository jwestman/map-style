This is a [MapLibre](https://maplibre.org/maplibre-gl-js-docs/style-spec/) map
style that aims to follow GNOME design guidelines.

## Goals

- **Compliance with GNOME's HIG.** The map style should follow GNOME's Human
Interface Guidelines. This encompasses typography, color scheme, icons, dark
mode, accessibility support, and overall design ethos.
- **Worldwide usefulness.** The map style should be suitable for use everywhere,
not just in a few regions or cultures. For example, a car-centric map would be
of little use in much of the world.
- **Compatibility with libshumate.** Libshumate is GNOME's map library, and its
vector tile support is somewhat limited. All changes **must** be tested against
libshumate (`preview.py` assists with this).
- **Simplicity.** OpenStreetMap Carto is great for mappers and for showing off
the rich variety of data in OSM, but the same design decisions make it a very
cluttered map poorly suited for everyday use.

## Technical Details

This map style is designed for use with the [OpenMapTiles schema](https://openmaptiles.org/schema/).
The repository itself contains a Python script which generates the JSON files.
This allows for code reuse and makes it easy to generate variants of the style
(e.g. for dark mode or high contrast).

## Contributing

### Running

Run `./preview.py` in the terminal to show a live preview. This requires
the standard GNOME toolchain (git, meson, ninja, a C compiler) and
[watchdog](https://pypi.org/project/watchdog/). The script downloads and compiles
libshumate (because sometimes development branches are needed), then listens for
changes to the `.py` files under the `style` directory and reloads the map file.

### Editing

The layers are defined in the `style` directory. For example, `style/water.py`
contains the definitions for water bodies, waterways, etc.

To add a layer, you must define it in a file, then import that file in
`style/__init__.py` if it's not already. Then, add the layer in the correct
order in the `layers` array.

### Distributing

`generate.py` takes the color scheme and contrast variant options and generates
a file in the `dist` directory. To generate all variants, run `generate.py`
separately for each one.

## Prior Art

There are plenty of existing map styles we can draw inspiration from. Many
of them are available on [maputnik](https://maputnik.github.io/).

Another interesting style is [OpenStreetMap Americana](https://github.com/ZeLonewolf/openstreetmap-americana).
It uses a similar technique of building the style in code (albeit JavaScript).
It is also pioneering the use of worldwide highway shields, and it is public
domain, so we can build on their work.

## License

This repository is licensed under the GPL version 3 or, at your option, any
later version.

Some files may be copied (with permission, of course) from other projects.
