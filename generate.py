#!/usr/bin/env python3
# generate.py
#
# Copyright 2022 James Westman <james@jwestman.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import json, os, re, sys

COLOR_SCHEME = None
CONTRAST = None

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("usage: ./generate.py <color scheme> <contrast>")
        exit(1)

    COLOR_SCHEME = sys.argv[1]
    CONTRAST = sys.argv[2]

    if COLOR_SCHEME not in ["light", "dark"]:
        print(f"expected 'light' or 'dark', not '{COLOR_SCHEME}'")
        exit(1)

    if CONTRAST not in ["hc", "none"]:
        print(f"expected 'hc' or 'none', not '{CONTRAST}'")
        exit(1)

    if CONTRAST == "none":
        name = f"{COLOR_SCHEME}"
    else:
        name = f"{COLOR_SCHEME}-hc"

    try:
        os.mkdir("dist")
    except:
        pass

    with open(f"dist/{name}.json", "w") as f:
        import style

        json.dump(style.map_style, f, indent=2)

    icons = []
    for file in os.listdir("icons"):
        if not re.match(r"^[a-z0-9-]+\.svg$", file):
            continue
        icons.append(file)
    with open("dist/icons.json", "w") as f:
        json.dump(icons, f)
