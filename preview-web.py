#!/usr/bin/env python3
# preview-web.py
#
# Copyright 2022 James Westman <james@jwestman.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import re
import subprocess, sys
from http.server import HTTPServer, BaseHTTPRequestHandler

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

if __name__ == "__main__":

    def send_reload():
        pass

    def regenerate():
        print("Regenerating map styles...")
        for color_scheme in ["light", "dark"]:
            for contrast in ["hc", "none"]:
                p = subprocess.run(
                    ["python3", "generate.py", color_scheme, contrast],
                    stderr=sys.stderr,
                )
                if p.returncode != 0:
                    print("Failed to regenerate map style")
                    return

        send_reload()

    class Handler(FileSystemEventHandler):
        def on_modified(self, event):
            if event.src_path.endswith(".py"):
                regenerate()

        def on_moved(self, event):
            if event.dest_path.endswith(".py"):
                regenerate()

    observer = Observer()
    observer.schedule(Handler(), "style", recursive=True)
    observer.start()

    regenerate()

    class Handler(BaseHTTPRequestHandler):
        def do_GET(self) -> None:
            if self.path == "/index.html":
                with open("public/index.html", "rb") as f:
                    self.send_response(200)
                    self.send_header("Content-Type", "text/html")
                    self.end_headers()
                    self.wfile.write(f.read())
            elif self.path == "/index.js":
                with open("public/index.js", "rb") as f:
                    self.send_response(200)
                    self.send_header("Content-Type", "application/javascript")
                    self.end_headers()
                    self.wfile.write(f.read())
            elif self.path == "/tiny-sdf.js":
                with open("public/tiny-sdf.js", "rb") as f:
                    self.send_response(200)
                    self.send_header("Content-Type", "application/javascript")
                    self.end_headers()
                    self.wfile.write(f.read())
            elif self.path.startswith("/dist/"):
                file = self.path.removeprefix("/dist/")
                if file not in [
                    "light.json",
                    "light-hc.json",
                    "dark.json",
                    "dark-hc.json",
                    "icons.json",
                ]:
                    self.send_response(404)
                    self.end_headers()
                    return
                with open(f"dist/{file}", "rb") as f:
                    self.send_response(200)
                    self.send_header("Content-Type", "application/json")
                    self.end_headers()
                    self.wfile.write(f.read())
            elif self.path.startswith("/icons/"):
                file = self.path.removeprefix("/icons/")
                if not re.match(r"^[a-z0-9-]+\.svg$", file):
                    self.send_response(404)
                    self.end_headers()
                    return

                try:
                    with open(f"icons/{file}", "rb") as f:
                        self.send_response(200)
                        self.send_header("Content-Type", "image/svg+xml")
                        self.end_headers()
                        self.wfile.write(f.read())
                except FileNotFoundError:
                    self.send_response(404)
                    self.end_headers()
            else:
                self.send_response(404)
                self.end_headers()

    with HTTPServer(("", 8000), Handler) as httpd:
        print("Serving on port 8000...")
        httpd.serve_forever()
