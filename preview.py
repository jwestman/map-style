#!/usr/bin/env python3
# preview.py
#
# Copyright 2022 James Westman <james@jwestman.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import subprocess, sys

import gi

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

if __name__ == "__main__":
    # Make sure libshumate is set up
    if not os.path.exists("libshumate/README.md"):
        print(
            "Please run `git submodule update --init` to initialize the libshumate submodule."
        )
        exit(1)

    # Build libshumate
    if not os.path.isdir("libshumate/_build"):
        subprocess.run(
            ["meson", "_build", "-Dvector_renderer=true"], cwd="libshumate", check=True
        )
    subprocess.run(["ninja", "-C", "_build"], cwd="libshumate", check=True)

    # Configure GIRepository to load libshumate from our submodule rather than the system
    gi.require_version("GIRepository", "2.0")
    from gi.repository import GIRepository

    GIRepository.Repository.prepend_library_path("libshumate/_build/shumate/")
    GIRepository.Repository.prepend_search_path("libshumate/_build/shumate/")

    gi.require_version("Adw", "1")
    gi.require_version("Gtk", "4.0")
    gi.require_version("Shumate", "1.0")
    from gi.repository import Adw, Gio, GLib, Gtk, Shumate

    Adw.init()
    # make sure the class is loaded before we use it in GtkBuilder
    Shumate.SimpleMap

    builder = Gtk.Builder.new_from_file("preview.ui")
    window = builder.get_object("window")

    color_scheme = "light"
    contrast = "none"

    def regenerate():
        print("Regenerating map styles...")

        gresource = '<?xml version="1.0" encoding="UTF-8"?>\n<gresources>\n  <gresource prefix="/net/jwestman/MapStyle/icons/scalable/actions">'
        for icon in os.listdir("icons"):
            if icon.endswith(".svg"):
                gresource += f"\n    <file>{icon}</file>"
        gresource += "\n  </gresource>\n</gresources>"
        with open("icons/map-style.gresource.xml", "w") as f:
            f.write(gresource)
        subprocess.run(
            [
                "glib-compile-resources",
                "--sourcedir",
                "icons",
                "icons/map-style.gresource.xml",
            ],
            stderr=sys.stderr,
        )

        for color_scheme in ["light", "dark"]:
            for contrast in ["hc", "none"]:
                p = subprocess.run(
                    ["python3", "generate.py", color_scheme, contrast],
                    stderr=sys.stderr,
                )
                if p.returncode != 0:
                    print("Failed to regenerate map style")
                    return
        set_map_source()

    hc_switch = builder.get_object("hc_switch")
    darkmode_switch = builder.get_object("darkmode_switch")
    map = builder.get_object("map")

    resource_bundle = None

    def set_map_source(*args):
        global resource_bundle

        color_scheme = "dark" if darkmode_switch.props.active else "light"
        if hc_switch.props.active:
            name = f"{color_scheme}-hc"
        else:
            name = f"{color_scheme}"

        with open(f"dist/{name}.json") as f:
            print("Showing", f"dist/{name}.json")
            try:
                if resource_bundle is not None:
                    resource_bundle._unregister()
                resource_bundle = Gio.Resource.load("icons/map-style.gresource")
                resource_bundle._register()

                map_source = Shumate.VectorRenderer.new(name, f.read())
                map_source.set_license(
                    "\u00A9 MapTiler \u00A9 OpenStreetMap contributors"
                )

                icontheme = Gtk.IconTheme()
                icontheme.add_resource_path("/net/jwestman/MapStyle/icons")
                sprite_sheet = Shumate.VectorSpriteSheet()
                for icon in os.listdir("icons"):
                    if icon.endswith(".svg"):
                        i = icontheme.lookup_icon(
                            icon.removesuffix(".svg"),
                            None,
                            16,
                            1,
                            Gtk.TextDirection.NONE,
                            0,
                        )
                        sprite = Shumate.VectorSprite.new(i)
                        sprite_sheet.add_sprite(icon.removesuffix(".svg"), sprite)
                map_source.set_sprite_sheet(sprite_sheet)

                map.set_map_source(map_source)
            except Exception as e:
                print("Failed to set map style:", e)

    hc_switch.connect("notify::active", set_map_source)
    darkmode_switch.connect("notify::active", set_map_source)

    zoom_label = builder.get_object("zoom_label")

    def set_zoom_label(*_args):
        zoom_label.set_label(f"Zoom: {map.get_viewport().get_zoom_level():0.2f}")

    map.get_viewport().connect("notify::zoom-level", set_zoom_label)

    window.show()

    class Handler(FileSystemEventHandler):
        def on_modified(self, event):
            if event.src_path.endswith(".py"):
                GLib.idle_add(regenerate)

        def on_moved(self, event):
            if event.dest_path.endswith(".py"):
                GLib.idle_add(regenerate)

    observer = Observer()
    observer.schedule(Handler(), "style", recursive=True)
    observer.start()

    regenerate()

    GLib.MainLoop().run()
