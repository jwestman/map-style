"use strict";

import TinySDF from "./tiny-sdf.js";

document.title = "Map Style Preview";

const mapContainer = document.getElementById("mapContainer");

const map = window.map = new maplibregl.Map({
  container: mapContainer,
  style: "dist/light.json",
  hash: true,
});

map.on("styledata", (event) => {
  const style = map.getStyle();
  for (const layer of style.layers) {
    if (!layer.metadata) continue;
    const cursor = layer.metadata["libshumate:cursor"];

    if (cursor) {
      map.on("mouseenter", layer.id, (event) => {
        const features = map.queryRenderedFeatures(event.point);
        if (features.length > 0) {
          const canvas = mapContainer.querySelector(".maplibregl-canvas");
          canvas.style.cursor = cursor;
        }
      });

      map.on("mouseleave", layer.id, () => {
        const canvas = mapContainer.querySelector(".maplibregl-canvas");
        canvas.style.cursor = "grab";
      });
    }
  }
});

for (const variantBtn of document.querySelectorAll(".variant")) {
  variantBtn.addEventListener("change", () => {
    map.setStyle(`dist/${variantBtn.dataset.variant}.json`);
  });
  if (variantBtn.checked) {
    variantBtn.dispatchEvent(new Event("change"));
  }
}

const tinySDF = new TinySDF();

fetch("dist/icons.json")
  .then((response) => response.json())
  .then((icons) => {
    for (const icon of icons) {
      fetch(`icons/${icon}`)
        .then((response) => response.text())
        .then((svg) => {
          const i = tinySDF.draw((ctx, buf) => {
            const image = new Image();
            image.src = `data:image/svg+xml;base64,${btoa(svg)}`;
            ctx.drawImage(image, buf, buf);
          });
          map.addImage(icon.replace(/\.svg$/, ''), i, {sdf: true});
        });
    }
  });
