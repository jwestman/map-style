# __init__.py
#
# Copyright 2022 James Westman <james@jwestman.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from .common import *
from . import (
    aerial,
    airports,
    boundaries,
    buildings,
    colors,
    housenumbers,
    landcover,
    places,
    rail,
    roads,
    water,
)

name = ("High Contrast " if HIGH_CONTRAST else "") + ("Dark" if DARK_MODE else "Light")


def layered_layers():
    for layer in range(-10, 11):
        if layer == 0:
            filter = ["any", ["==", ["get", "layer"], 0], ["!", ["has", "layer"]]]
        else:
            filter = ["==", ["get", "layer"], layer]

        yield from roads.roads(layer, filter)
        yield from rail.rail(layer, filter)
        yield from aerial.aerial(layer, filter)

        if layer == 0:
            yield airports.runway_fill
            yield airports.runway_line
            yield airports.taxiway_line
            yield buildings.buildings
            yield buildings.buildings_outline


layers = [
    {
        "id": "background",
        "type": "background",
        "paint": {"background-color": colors.background},
    },
    *landcover.landcover,
    water.water_fill,
    water.water_line,
    boundaries.boundaries,
    *layered_layers(),
    water.ferry_line,
    water.water_name,
    water.water_name_line,
    water.waterway_name,
    water.ferry_line_name,
    housenumbers.housenumbers,
    roads.road_symbol,
    roads.junction_symbol,
    aerial.aerial_labels,
    places.places,
]


def flatten(l):
    if isinstance(l, list):
        return [c for e in l for c in flatten(e) if c is not None]
    else:
        return [l]


map_style = {
    "version": 8,
    "name": name,
    "sources": {
        "vector-tiles": {
            "type": "vector",
            "tiles": [
                "https://tiles.maps.jwestman.net/data/streets_v3/{z}/{x}/{y}.pbf"
            ],
            "minzoom": 0,
            "maxzoom": 14,
        }
    },
    "glyphs": "https://tiles.maps.jwestman.net/fonts/{fontstack}/{range}.pbf",
    "layers": flatten(layers),
}

__all__ = ["map_style"]
