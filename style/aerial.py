# aerial.py
#
# Copyright 2023 Marcus Lundblad <ml@dfupdate.se>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Aerailway transport: https://wiki.openstreetmap.org/wiki/Map_features#Aerialway

from .common import *

color = rgb(145, 116, 123) if DARK_MODE else rgb(200, 146, 153)
label_color = mix(color, rgb(255, 255, 255), 0.3) if DARK_MODE else darken(color, 0.6)

cable_car_filter = [
    "all",
    ["==", ["get", "class"], "aerialway"],
    [
        "in",
        ["get", "subclass"],
        [
            "literal",
            [
                "cable_car",
                "gondola",
                "mixed_lift",
            ],
        ],
    ],
]

lift_filter = [
    "all",
    ["==", ["get", "class"], "aerialway"],
    [
        "in",
        ["get", "subclass"],
        [
            "literal",
            [
                "chair_lift",
                "drag_lift",
                "t-bar",
                "j-bar",
                "platter",
                "rope_tow",
                "zip-line",
            ],
        ],
    ],
]


def aerial(layer, layer_filter):
    cable_car = [
        line(
            f"cable-car-{layer}",
            "transportation",
            filter=[
                "all",
                cable_car_filter,
                layer_filter,
            ],
            line_color=color,
            line_width={
                "stops": [
                    [8, 0.25],
                    [13, 0.5],
                    [16, 2],
                ]
            },
        ),
        line(
            f"cable-car-{layer}-ties",
            "transportation",
            filter=["all", cable_car_filter, layer_filter],
            line_color=color,
            line_dasharray=[0.3, 5],
            line_width={
                "stops": [
                    [13, 2],
                    [16, 10],
                ]
            },
            minzoom=13,
        ),
    ]

    lift = [
        line(
            f"lift-{layer}",
            "transportation",
            filter=[
                "all",
                lift_filter,
                layer_filter,
            ],
            line_color=color,
            line_width={
                "stops": [
                    [8, 0.25],
                    [13, 0.5],
                    [16, 2],
                ]
            },
        ),
        line(
            f"lift-{layer}-ties",
            "transportation",
            filter=["all", lift_filter, layer_filter],
            line_color=color,
            line_dasharray=[0.3, 10],
            line_width={
                "stops": [
                    [13, 2],
                    [16, 10],
                ]
            },
            minzoom=13,
        ),
    ]

    return [*cable_car, *lift]


aerial_labels = symbol(
    f"aerial-labels",
    "transportation_name",
    filter=["any", cable_car_filter, lift_filter],
    symbol_placement="line",
    text_field=localized_name,
    text_color=label_color,
    text_size=15,
    minzoom=13,
)
