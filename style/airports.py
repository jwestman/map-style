# airports.py
#
# Copyright 2022 James Westman <james@jwestman.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from .common import *

runway_color = rgb(45, 40, 50) if DARK_MODE else rgb(215, 205, 220)

runway_line = line(
    "runway-line",
    "aeroway",
    filter=[
        "all",
        is_linestring,
        ["==", ["get", "class"], "runway"],
    ],
    line_color=runway_color,
    line_width={
        "base": 2,
        "stops": [
            [10, 4],
            [18, 100],
        ],
    },
    minzoom=11,
)

taxiway_line = line(
    "taxiway-line",
    "aeroway",
    filter=[
        "all",
        is_linestring,
        ["==", ["get", "class"], "taxiway"],
    ],
    line_color=runway_color,
    line_width={
        "base": 2,
        "stops": [
            [10, 2],
            [18, 10],
        ],
    },
    minzoom=11,
)

runway_fill = fill(
    "runway-fill",
    "aeroway",
    filter=[
        "all",
        is_polygon,
        ["in", ["get", "class"], ["literal", ["runway", "taxiway"]]],
    ],
    fill_color=runway_color,
    minzoom=12,
)
