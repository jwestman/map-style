# boundaries.py
#
# Copyright 2022 James Westman <james@jwestman.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from .common import *


def boundary(level, width, dotted):
    def boundary_line(disputed):
        if disputed:
            dasharray = [1.5 * 4]
        elif dotted:
            dasharray = [0, 1.5]
        else:
            dasharray = None

        # Show disputed maritime boundaries
        if disputed:
            disputed_filter = ["==", "disputed", 1]
            suffix = "_disputed"
        else:
            disputed_filter = ["all", ["!=", "disputed", 1], ["!=", "maritime", 1]]
            suffix = ""

        return line(
            f"admin_{level}_boundary{suffix}",
            "boundary",
            filter=[
                "all",
                ["==", "admin_level", level],
                disputed_filter,
            ],
            minzoom=level - 1,
            line_color=Colors.light4 if DARK_MODE else Colors.dark1,
            line_join="round",
            line_cap="round",
            line_dasharray=dasharray,
            line_width={
                "stops": [
                    [level, width / 5],
                    [level + 14, width * 10 / 5],
                ]
            },
        )

    return [boundary_line(True), boundary_line(False)]


# (admin level, width, dotted)
# See https://wiki.openstreetmap.org/wiki/Tag:boundary%253Dadministrative
boundary_settings = [
    # Countries
    (2, 2, False),
    # Major, generally semi-autonomous regions of countries
    (3, 3, True),
    # States, provinces, etc.
    (4, 2, True),
    # Counties, cities, etc. based on country
    (5, 1, True),
    (6, 0.8, True),
    (7, 0.6, True),
    (8, 0.4, True),
]

boundaries = [boundary(*args) for args in boundary_settings]
