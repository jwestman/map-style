# buildings.py
#
# Copyright 2023 James Westman <james@jwestman.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from .common import *
from . import colors

color = rgb(70, 70, 70) if DARK_MODE else rgb(180, 180, 180)

buildings = fill(
    "buildings",
    "building",
    {
        "stops": [
            [14, mix(color, colors.background, 0.2 if DARK_MODE else 0.1)],
            [18, mix(color, colors.background, 1)],
        ]
    },
    fill_opacity={
        "stops": [
            [14, 0.5],
            [15, 1],
        ]
    },
)

buildings_outline = line(
    "buildings-outline",
    "building",
    darken(color, 1.33 if DARK_MODE else 0.75),
    line_width={
        "stops": [
            [15, 0.05],
            [18, 1 if DARK_MODE else 0.5],
        ]
    },
    minzoom=15,
)
