# common.py
#
# Copyright 2022 James Westman <james@jwestman.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import typing as T
from enum import Enum
from __main__ import COLOR_SCHEME, CONTRAST

DARK_MODE = COLOR_SCHEME == "dark"
HIGH_CONTRAST = CONTRAST == "hc"

is_point = [
    "in",
    ["geometry-type"],
    ["literal", ["Point", "MultiPoint"]],
]

is_linestring = [
    "in",
    ["geometry-type"],
    ["literal", ["LineString", "MultiLineString"]],
]

is_polygon = [
    "in",
    ["geometry-type"],
    ["literal", ["Polygon", "MultiPolygon"]],
]


def rgb(r, g, b):
    return f"#{int(min(r, 255)):02x}{int(min(g, 255)):02x}{int(min(b, 255)):02x}"


def parse_color(rgb: str) -> T.Tuple[int, int, int]:
    return int(rgb[1:3], 16), int(rgb[3:5], 16), int(rgb[5:], 16)


def darken(color: str, mul: float) -> str:
    r, g, b = parse_color(color)
    return rgb(r * mul, g * mul, b * mul)


def mix(color1: str, color2: str, amount: float) -> str:
    amount2 = 1 - amount
    r1, g1, b1 = parse_color(color1)
    r2, g2, b2 = parse_color(color2)
    return rgb(
        r1 * amount + r2 * amount2,
        g1 * amount + g2 * amount2,
        b1 * amount + b2 * amount2,
    )


class Colors:
    blue1 = rgb(153, 193, 241)
    blue2 = rgb(98, 160, 234)
    blue3 = rgb(53, 132, 228)
    blue4 = rgb(28, 113, 216)
    blue5 = rgb(26, 95, 180)
    green1 = rgb(143, 240, 164)
    green2 = rgb(87, 227, 137)
    green3 = rgb(51, 209, 122)
    green4 = rgb(46, 194, 126)
    green5 = rgb(38, 162, 105)
    yellow1 = rgb(249, 240, 107)
    yellow2 = rgb(248, 228, 92)
    yellow3 = rgb(246, 211, 45)
    yellow4 = rgb(245, 194, 17)
    yellow5 = rgb(229, 165, 10)
    orange1 = rgb(255, 190, 111)
    orange2 = rgb(255, 163, 72)
    orange3 = rgb(255, 120, 0)
    orange4 = rgb(230, 97, 0)
    orange5 = rgb(198, 70, 0)
    red1 = rgb(246, 97, 81)
    red2 = rgb(237, 51, 59)
    red3 = rgb(224, 27, 36)
    red4 = rgb(192, 28, 40)
    red5 = rgb(165, 29, 45)
    purple1 = rgb(220, 138, 221)
    purple2 = rgb(192, 97, 203)
    purple3 = rgb(145, 65, 172)
    purple4 = rgb(129, 61, 156)
    purple5 = rgb(97, 53, 131)
    brown1 = rgb(205, 171, 143)
    brown2 = rgb(181, 131, 90)
    brown3 = rgb(152, 106, 68)
    brown4 = rgb(134, 94, 60)
    brown5 = rgb(99, 69, 44)
    light1 = rgb(255, 255, 255)
    light2 = rgb(246, 245, 244)
    light3 = rgb(222, 221, 218)
    light4 = rgb(192, 191, 188)
    light5 = rgb(154, 153, 150)
    dark1 = rgb(119, 118, 123)
    dark2 = rgb(94, 92, 100)
    dark3 = rgb(61, 56, 70)
    dark4 = rgb(36, 31, 49)
    dark5 = rgb(0, 0, 0)

    fg1 = light1 if DARK_MODE else dark5
    fg2 = light2 if DARK_MODE else dark4
    fg3 = light3 if DARK_MODE else dark3
    fg4 = light4 if DARK_MODE else dark2
    fg5 = light5 if DARK_MODE else dark1

    bg1 = dark1 if DARK_MODE else light5
    bg2 = dark2 if DARK_MODE else light4
    bg3 = dark3 if DARK_MODE else light3
    bg4 = dark4 if DARK_MODE else light2
    bg5 = dark5 if DARK_MODE else light1


def filter_some(d):
    if d is None:
        return None
    return {k: v for k, v in d.items() if v is not None}


def layer(
    id,
    source_layer,
    type,
    paint=None,
    layout=None,
    source=None,
    filter=None,
    minzoom=None,
    maxzoom=None,
    metadata=None,
):
    return filter_some(
        {
            "id": id,
            "type": type,
            "source": source or "vector-tiles",
            "source-layer": source_layer or "vector-tiles",
            "filter": filter,
            "paint": filter_some(paint),
            "layout": filter_some(layout),
            "minzoom": minzoom,
            "maxzoom": maxzoom,
            "metadata": filter_some(metadata),
        }
    )


def line(
    id,
    source_layer,
    line_color=None,
    line_opacity=None,
    line_width=None,
    line_join=None,
    line_cap=None,
    line_dasharray=None,
    **kwargs,
):
    return layer(
        id,
        source_layer,
        "line",
        paint={
            "line-color": line_color,
            "line-opacity": line_opacity,
            "line-width": line_width,
            "line-dasharray": line_dasharray,
        },
        layout={
            "line-join": line_join,
            "line-cap": line_cap,
        },
        **kwargs,
    )


def fill(
    id,
    source_layer,
    fill_color=None,
    fill_opacity=None,
    **kwargs,
):
    return layer(
        id,
        source_layer,
        "fill",
        paint={
            "fill-color": fill_color,
            "fill-opacity": fill_opacity,
        },
        **kwargs,
    )


def symbol(
    id,
    source_layer,
    text_anchor=None,
    text_field=None,
    text_color=None,
    text_size=None,
    text_offset=None,
    text_padding=None,
    text_font=None,
    text_transform=None,
    icon_allow_overlap=None,
    icon_ignore_placement=None,
    icon_color=None,
    icon_image=None,
    icon_opacity=None,
    icon_rotate=None,
    icon_size=None,
    cursor="pointer",
    symbol_placement=None,
    symbol_spacing=None,
    **kwargs,
):
    return layer(
        id,
        source_layer,
        "symbol",
        layout={
            "icon-allow-overlap": icon_allow_overlap,
            "icon-ignore-placement": icon_ignore_placement,
            "icon-image": icon_image,
            "icon-rotate": icon_rotate,
            "icon-size": icon_size,
            "text-anchor": text_anchor,
            "text-field": text_field,
            "text-size": text_size,
            "text-offset": text_offset,
            "text-padding": text_padding,
            "text-font": text_font or ["Cantarell Regular"],
            "text-transform": text_transform,
            "symbol-placement": symbol_placement,
            "symbol-spacing": symbol_spacing,
        },
        paint={
            "text-color": text_color,
            "icon-color": icon_color,
            "icon-opacity": icon_opacity,
        },
        metadata={
            "libshumate:cursor": cursor,
        },
        **kwargs,
    )


locale = [
    "let",
    "locale_tag",
    ["resolved-locale", ["collator", {}]],
    [
        "case",
        ["!=", -1, ["index-of", "-", ["var", "locale_tag"]]],
        [
            "slice",
            ["var", "locale_tag"],
            0,
            ["index-of", "-", ["var", "locale_tag"]],
        ],
        ["var", "locale_tag"],
    ],
]

# The to-string is necessary in MapLibre GL JS because of how its type coercion works.
localized_name = [
    "to-string",
    [
        "coalesce",
        # special case for Norwegian (Bokmål "nb" and nynorsk "nn") with the
        # fallback language code "no" for names with a common translation:
        # https://wiki.openstreetmap.org/wiki/Multilingual_names#Norway
        [
            "match",
            locale,
            "nb",
            ["coalesce", ["get", "name:nb"], ["get", "name:no"]],
            "nn",
            ["coalesce", ["get", "name:nn"], ["get", "name:no"]],
            [
                "get",
                [
                    "concat",
                    "name:",
                    locale,
                ],
            ],
        ],
        ["get", "name"],
    ],
]
