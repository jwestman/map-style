# housenumbers.py
#
# Copyright 2023 James Westman <james@jwestman.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from .common import *

color = Colors.light5 if DARK_MODE else Colors.dark1

housenumbers = symbol(
    "housenumbers",
    "housenumber",
    text_field=["get", "housenumber"],
    text_size={
        "stops": [
            [18, 9],
            [20, 11],
        ]
    },
    text_color=color,
    minzoom=18,
)
