# landcover.py
#
# Copyright 2023 James Westman <james@jwestman.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from .common import *
from . import colors


def landcover_layer(landcover_class: str, dark_color: str, color: str):
    return fill(
        f"landcover-{landcover_class}",
        "landcover",
        filter=["==", ["get", "class"], landcover_class],
        fill_color={
            "stops": [
                [7, colors.background],
                [10, dark_color if DARK_MODE else color],
            ]
        },
    )


landcover = [
    landcover_layer("farmland", rgb(38, 36, 25), rgb(232, 231, 208)),
    landcover_layer("ice", rgb(35, 36, 49), rgb(226, 225, 255)),
    landcover_layer("grass", rgb(51, 64, 52), rgb(173, 204, 179)),
    landcover_layer("wetland", rgb(30, 38, 39), rgb(204, 217, 215)),
    landcover_layer("wood", rgb(41, 52, 42), rgb(163, 194, 169)),
    landcover_layer("rock", rgb(35, 36, 35), rgb(212, 211, 208)),
    landcover_layer("sand", rgb(47, 40, 30), rgb(242, 227, 203)),
]

__all__ = ["landcover"]
