# places.py
#
# Copyright 2022 James Westman <james@jwestman.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from .common import *

continent = symbol(
    "continent_symbol",
    "place",
    filter=["==", "class", "continent"],
    text_field=localized_name,
    text_size={
        "stops": [
            [0, 18],
            [1, 24],
        ]
    },
    text_font=["Cantarell Light"],
    text_color=Colors.fg3,
    maxzoom=1.99,
)

country = symbol(
    "country_symbol",
    "place",
    filter=[
        "all",
        ["==", "class", "country"],
        # I'm not sure why OpenMapTiles has both a country and a continent for
        # Antarctica
        ["!=", "name", "Antarctica"],
    ],
    text_field=localized_name,
    text_size={
        "stops": [
            [1, 14],
            [3, 16],
            [4, 20],
            [5, 24],
            [6, 28],
        ]
    },
    text_font=["Cantarell Extrabold"],
    text_color=Colors.fg3,
    maxzoom=6,
)

state = symbol(
    "state_symbol",
    "place",
    filter=["in", "class", "state", "province"],
    text_field=localized_name,
    text_size={
        "stops": [
            [4, 14],
            [6, 20],
        ]
    },
    text_font=["Cantarell Bold"],
    text_color=Colors.fg4,
    text_transform="uppercase",
    minzoom=4,
    maxzoom=8,
)

city = symbol(
    "city_symbol",
    "place",
    filter=["==", "class", "city"],
    text_field=localized_name,
    text_size={
        "stops": [
            [4, 10],
            [6, 16],
            [12, 24],
        ]
    },
    text_font=["Cantarell Bold"],
    text_color=Colors.fg1,
    minzoom=4,
    maxzoom=12,
)

town = symbol(
    "town_symbol",
    "place",
    filter=["in", "class", "town", "village"],
    text_field=localized_name,
    text_size={
        "stops": [
            [9, 12],
            [12, 18],
        ]
    },
    text_font=["Cantarell Bold"],
    text_color=Colors.fg1,
    maxzoom=13,
)

neighborhood = symbol(
    "neighborhood_symbol",
    "place",
    filter=["in", "class", "hamlet", "suburb", "quarter", "neighborhood"],
    text_field=localized_name,
    text_size={
        "stops": [
            [12, 12],
            [15, 18],
        ]
    },
    text_font=["Cantarell Bold"],
    text_color=Colors.fg4,
    text_transform="uppercase",
    maxzoom=15,
)

places = [
    neighborhood,
    town,
    city,
    state,
    country,
    continent,
]
