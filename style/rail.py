# rail.py
#
# Copyright 2023 James Westman <james@jwestman.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from .common import *
from . import colors


def rail(layer, layer_filter):
    color = rgb(145, 116, 123) if DARK_MODE else rgb(200, 146, 153)
    tunnel_color = mix(color, colors.background, 0.7)
    bridge_color = darken(color, 0.75)

    light_rail_filter = [
        "any",
        [
            "all",
            ["==", ["get", "class"], "rail"],
            ["!=", ["get", "subclass"], "rail"],
        ],
        [
            "all",
            ["==", ["get", "class"], "transit"],
            [
                "in",
                ["get", "subclass"],
                ["literal", ["light_rail", "monorail", "funicular", "tram"]],
            ],
        ],
    ]

    light_rail = [
        line(
            f"light-rail-{layer}-bridge-casing",
            "transportation",
            filter=[
                "all",
                light_rail_filter,
                layer_filter,
                ["==", ["get", "brunnel"], "bridge"],
            ],
            line_color=bridge_color,
            line_width={
                "stops": [
                    [8, 0.75],
                    [13, 2],
                    [16, 6],
                ]
            },
        ),
        line(
            f"light-rail-{layer}",
            "transportation",
            filter=[
                "all",
                light_rail_filter,
                layer_filter,
                ["!=", ["get", "brunnel"], "tunnel"],
            ],
            line_color=color,
            line_width={
                "stops": [
                    [8, 0.25],
                    [13, 0.5],
                    [16, 2],
                ]
            },
        ),
        line(
            f"light-rail-{layer}-tunnel",
            "transportation",
            filter=[
                "all",
                light_rail_filter,
                layer_filter,
                ["==", ["get", "brunnel"], "tunnel"],
            ],
            line_color=tunnel_color,
            line_dasharray=[1, 0.5],
            line_width={
                "stops": [
                    [8, 0.25],
                    [13, 0.5],
                    [16, 2],
                ]
            },
        ),
        line(
            f"light-rail-{layer}-ties",
            "transportation",
            filter=["all", light_rail_filter, layer_filter],
            line_color=[
                "case",
                ["==", ["get", "brunnel"], "tunnel"],
                tunnel_color,
                color,
            ],
            line_dasharray=[0.3, 1.5],
            line_width={
                "stops": [
                    [13, 0.5],
                    [16, 4],
                ]
            },
            minzoom=13,
        ),
    ]

    heavy_rail_filter = [
        "all",
        [
            "any",
            [
                "all",
                ["==", ["get", "class"], "rail"],
                ["==", ["get", "subclass"], "rail"],
            ],
            [
                "all",
                ["==", ["get", "class"], "transit"],
                ["==", ["get", "subclass"], "subway"],
            ],
        ],
        ["!=", ["get", "service"], "yard"],
    ]

    heavy_rail = [
        line(
            f"heavy-rail-{layer}-bridge-casing",
            "transportation",
            filter=[
                "all",
                heavy_rail_filter,
                layer_filter,
                ["==", ["get", "brunnel"], "bridge"],
            ],
            line_color=bridge_color,
            line_width={
                "stops": [
                    [14, 0.5],
                    [16, 8],
                ]
            },
            minzoom=13,
        ),
        line(
            f"heavy-rail-{layer}",
            "transportation",
            filter=[
                "all",
                heavy_rail_filter,
                layer_filter,
                ["!=", ["get", "brunnel"], "tunnel"],
            ],
            line_color=color,
            line_width={
                "stops": [
                    [14, 0.5],
                    [16, 4],
                ]
            },
        ),
        line(
            f"heavy-rail-{layer}-tunnel",
            "transportation",
            filter=[
                "all",
                heavy_rail_filter,
                layer_filter,
                ["==", ["get", "brunnel"], "tunnel"],
            ],
            line_color=tunnel_color,
            line_dasharray=[2, 0.5],
            line_width={
                "stops": [
                    [14, 0.5],
                    [16, 4],
                ]
            },
        ),
        line(
            f"heavy-rail-{layer}-ties",
            "transportation",
            filter=[
                "all",
                heavy_rail_filter,
                layer_filter,
                ["!=", ["get", "brunnel"], "tunnel"],
            ],
            line_color=colors.background,
            line_dasharray=[2, 2],
            line_width={
                "stops": [
                    [14, 0.25],
                    [16, 2],
                ]
            },
            minzoom=14,
        ),
    ]

    return [
        *light_rail,
        *heavy_rail,
    ]
