# roads.py
#
# Copyright 2023 James Westman <james@jwestman.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from .common import *


def roads(layer, layer_filter):
    casings = []
    surfaces = []

    def stops(size: float, casing_width: float):
        return [
            [5, 0.15 * size],
            [9, 0.35 * size],
            [12, 0.65 * size + casing_width * 0.25],
            [14, 1 * size + casing_width * 0.45],
            [16, 2 * size + casing_width * 0.75],
            [18, 8 * size + casing_width],
            [19, 20 * size + casing_width],
            [20, 40 * size + casing_width],
            [21, 80 * size + casing_width],
            [22, 160 * size + casing_width],
        ]

    def road(
        name: str,
        filter,
        color: str,
        dark_color: str,
        size: float = 1,
        casing_minzoom: int = 12,
        casing_scale: float = 1,
    ):
        def road_stops(casing_width: float, fill: bool = False):
            casing_width *= casing_scale
            line_size = 0 if fill else size
            return stops(line_size, casing_width)

        def casing(alt, casing_filter, color, line_cap, stop_width, dasharray=None):
            for geometry_type, geometry_name in [
                (["LineString", "MultiLineString"], ""),
                (["Polygon", "MultiPolygon"], "-fill"),
            ]:
                casings.append(
                    line(
                        f"{name}-{layer}{alt}{geometry_name}casing",
                        "transportation",
                        filter=[
                            "all",
                            layer_filter,
                            filter,
                            casing_filter,
                            ["in", ["geometry-type"], ["literal", geometry_type]],
                            ["!=", ["get", "surface"], "unpaved"],
                        ],
                        line_color=color,
                        line_cap=line_cap,
                        line_width={
                            "base": 1.2,
                            "stops": road_stops(stop_width, geometry_name == "-fill"),
                        },
                        line_dasharray=dasharray,
                        minzoom=casing_minzoom,
                    )
                )

        casing_color = (
            mix(dark_color, rgb(255, 255, 255), 0.9)
            if DARK_MODE
            else darken(color, 0.9)
        )

        casing(
            "",
            ["!", ["has", "brunnel"]],
            casing_color,
            "round",
            3,
        )
        casing(
            "-bridge",
            ["==", ["get", "brunnel"], "bridge"],
            mix(dark_color, rgb(255, 255, 255), 0.75)
            if DARK_MODE
            else darken(color, 0.75),
            "butt",
            4,
        )
        casing(
            "-tunnel",
            ["==", ["get", "brunnel"], "tunnel"],
            mix(dark_color, rgb(255, 255, 255), 0.7)
            if DARK_MODE
            else darken(color, 0.8),
            "butt",
            4,
            dasharray=[0.5, 0.25],
        )

        surfaces.append(
            line(
                f"{name}-{layer}-unpaved-casing",
                "transportation",
                filter=[
                    "all",
                    is_linestring,
                    layer_filter,
                    filter,
                    ["==", ["get", "surface"], "unpaved"],
                ],
                line_color=casing_color,
                line_cap="round",
                line_width={
                    "base": 1.2,
                    "stops": stops(size * 1.3, 0),
                },
                line_dasharray=[1.5 / 1.3, 2 / 1.3],
                minzoom=5,
            )
        )

        surface_color = [
            "case",
            ["!=", ["get", "brunnel"], "tunnel"],
            dark_color if DARK_MODE else color,
            mix(dark_color, rgb(255, 255, 255), 0.85)
            if DARK_MODE
            else mix(color, rgb(255, 255, 255), 0.7),
        ]

        surfaces.append(
            line(
                f"{name}-{layer}",
                "transportation",
                filter=[
                    "all",
                    is_linestring,
                    layer_filter,
                    filter,
                    ["!=", ["get", "surface"], "unpaved"],
                ],
                line_color=surface_color,
                line_cap="round",
                line_width={
                    "base": 1.2,
                    "stops": road_stops(0),
                },
                minzoom=5,
            )
        )

        surfaces.append(
            line(
                f"{name}-{layer}-unpaved",
                "transportation",
                filter=[
                    "all",
                    is_linestring,
                    layer_filter,
                    filter,
                    ["==", ["get", "surface"], "unpaved"],
                ],
                line_color=surface_color,
                line_cap="round",
                line_width={
                    "base": 1.2,
                    "stops": stops(size, 0),
                },
                line_dasharray=[1.5, 2],
                minzoom=5,
            )
        )

        surfaces.append(
            fill(
                f"{name}-{layer}-fill",
                "transportation",
                filter=[
                    "all",
                    is_polygon,
                    layer_filter,
                    filter,
                ],
                fill_color=surface_color,
            )
        )

    road(
        "pedestrian",
        [
            "all",
            ["==", ["get", "class"], "path"],
            ["==", ["get", "subclass"], "pedestrian"],
        ],
        rgb(190, 189, 200),
        rgb(37, 36, 42),
        0.75,
    )

    road(
        "service",
        ["in", ["get", "class"], ["literal", ["service"]]],
        rgb(200, 199, 180),
        rgb(42, 41, 36),
        0.5,
        casing_minzoom=14 if DARK_MODE else None,
        casing_scale=0.4,
    )

    road(
        "minor",
        ["in", ["get", "class"], ["literal", ["tertiary", "minor"]]],
        rgb(215, 210, 188),
        rgb(65, 63, 57),
        casing_minzoom=14 if DARK_MODE else None,
    )

    road(
        "secondary",
        ["in", ["get", "class"], ["literal", ["secondary"]]],
        rgb(245, 210, 86),
        rgb(99, 69, 44),
        1.5,
    )

    road(
        "trunk",
        ["in", ["get", "class"], ["literal", ["trunk", "primary"]]],
        rgb(245, 194, 17),
        rgb(122, 85, 54),
        1.75,
    )

    road(
        "motorway",
        ["in", ["get", "class"], ["literal", ["motorway"]]],
        rgb(229, 165, 10),
        rgb(152, 106, 68),
        2,
    )

    surfaces.append(
        line(
            f"path-{layer}",
            "transportation",
            "#5e5c64" if DARK_MODE else "#9a9996",
            line_width={
                "base": 1.2,
                "stops": stops(0.2, 0),
            },
            filter=[
                "all",
                layer_filter,
                is_linestring,
                ["==", ["get", "class"], "path"],
                ["!=", ["get", "subclass"], "pedestrian"],
                ["!=", ["get", "surface"], "unpaved"],
            ],
        )
    )

    surfaces.append(
        line(
            f"path-{layer}-unpaved",
            "transportation",
            "#5e5c64" if DARK_MODE else "#9a9996",
            line_width={
                "base": 1.2,
                "stops": stops(0.2, 0),
            },
            filter=[
                "all",
                layer_filter,
                is_linestring,
                ["==", ["get", "class"], "path"],
                ["!=", ["get", "subclass"], "pedestrian"],
                ["==", ["get", "surface"], "unpaved"],
            ],
            line_cap="round",
            line_dasharray=[3, 3],
        )
    )
    surfaces.append(
        fill(
            f"path-{layer}-fill",
            "transportation",
            "#5e5c64" if DARK_MODE else "#9a9996",
            filter=[
                "all",
                layer_filter,
                is_polygon,
                ["==", ["get", "class"], "path"],
                ["!=", ["get", "subclass"], "pedestrian"],
            ],
        )
    )

    oneway = symbol(
        f"oneway-{layer}",
        "transportation",
        icon_image="arrow1-right-symbolic",
        icon_color=Colors.light1 if DARK_MODE else Colors.dark5,
        icon_rotate=["match", ["get", "oneway"], 1, 0, 180],
        icon_opacity=0.25,
        icon_size=[
            "let",
            "base",
            [
                "match",
                ["get", "class"],
                ["motorway", "trunk", "primary"],
                0.75,
                ["secondary", "tertiary"],
                0.6,
                0.5,
            ],
            [
                "interpolate",
                ["linear"],
                ["zoom"],
                14,
                ["*", ["var", "base"], 0.75],
                16,
                ["*", ["var", "base"], 1.25],
            ],
        ],
        icon_allow_overlap=True,
        icon_ignore_placement=True,
        symbol_placement="line",
        symbol_spacing=350,
        filter=[
            "all",
            is_linestring,
            [
                "!=",
                [
                    "slice",
                    ["get", "class"],
                    ["-", ["length", ["get", "class"]], ["length", "_construction"]],
                ],
                "_construction",
            ],
            ["in", ["get", "oneway"], ["literal", [1, -1]]],
            layer_filter,
        ],
        minzoom=16,
    )

    return [*casings, *surfaces, oneway]


road_symbol = symbol(
    "highway_name",
    "transportation_name",
    text_field=localized_name,
    text_size=12,
    text_color=Colors.light1 if DARK_MODE else Colors.dark5,
    symbol_placement="line",
    filter=[
        "in",
        ["get", "class"],
        [
            "literal",
            [
                "motorway",
                "trunk",
                "primary",
                "secondary",
                "tertiary",
                "minor",
                "service",
                "path",
            ],
        ],
    ],
    minzoom=10,
)

junction_color = {
    "stops": [
        [13, Colors.light3 if DARK_MODE else Colors.dark2],
        [16, Colors.light1 if DARK_MODE else Colors.dark5],
    ]
}

junction_symbol = symbol(
    "highway_junction",
    "transportation_name",
    text_field=["get", "ref"],
    text_offset=[0.7, 0],
    text_anchor="left",
    text_font=["Cantarell Bold"],
    icon_image="arrow2-top-right-symbolic",
    icon_size={
        "stops": [
            [13, 0.5],
            [16, 0.75],
        ]
    },
    text_size={
        "stops": [
            [13, 8],
            [16, 12],
        ]
    },
    text_color=junction_color,
    icon_color=junction_color,
    filter=[
        "all",
        ["has", "ref"],
        ["==", ["get", "subclass"], "junction"],
        [
            "in",
            ["get", "class"],
            [
                "literal",
                [
                    "motorway",
                    "trunk",
                    "primary",
                    "secondary",
                    "tertiary",
                    "minor",
                    "service",
                    "path",
                ],
            ],
        ],
    ],
    minzoom=12,
)
