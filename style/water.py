# water.py
#
# Copyright 2022 James Westman <james@jwestman.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from .common import *

water_color = rgb(15, 47, 94) if DARK_MODE else rgb(153, 193, 241)
water_label_color = (
    mix(water_color, rgb(255, 255, 255), 0.6) if DARK_MODE else darken(water_color, 0.7)
)
ferry_label_color = (
    mix(water_label_color, rgb(255, 255, 255), 0.5)
    if DARK_MODE
    else darken(water_label_color, 0.5)
)

water_label_size = [
    "match",
    ["get", "class"],
    "ocean",
    18,
    "sea",
    16,
    "river",
    14,
    "lake",
    14,
    10,
]

water_fill = fill(
    "water-fill",
    "water",
    fill_color=water_color,
)

water_line = line(
    "water-line",
    "waterway",
    line_color=water_color,
    line_width={
        "stops": [
            [4, 1],
            [14, 2],
            [18, 8],
        ]
    },
)

water_name = symbol(
    "water-name",
    "water_name",
    filter=is_point,
    text_field=localized_name,
    text_color=water_label_color,
    text_size=water_label_size,
)

water_name_line = symbol(
    "water-name-line",
    "water_name",
    filter=["in", ["geometry-type"], ["literal", ["MultiLineString", "LineString"]]],
    symbol_placement="line",
    text_field=localized_name,
    text_color=water_label_color,
    text_size=water_label_size,
)

waterway_name = symbol(
    "waterway-name",
    "waterway",
    filter=["in", ["geometry-type"], ["literal", ["MultiLineString", "LineString"]]],
    symbol_placement="line",
    text_field=localized_name,
    text_color=water_label_color,
    text_size=water_label_size,
)

ferry_line = line(
    "ferry-line",
    "transportation",
    filter=["==", ["get", "class"], "ferry"],
    line_color=water_label_color,
    line_dasharray=[5, 3],
    line_width={
        "stops": [
            [11, 0.5],
            [16, 2],
        ]
    },
    minzoom=11,
)

ferry_line_name = symbol(
    "ferry-line-name",
    "transportation_name",
    text_field=localized_name,
    text_size=15,
    text_color=ferry_label_color,
    symbol_placement="line",
    filter=[
        "in",
        ["get", "class"],
        [
            "literal",
            ["ferry"],
        ],
    ],
    minzoom=11,
)
